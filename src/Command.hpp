#ifndef COMMAND_HPP_
#define COMMAND_HPP_
#include <string>
#include <cstring>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <boost/tokenizer.hpp>
#include <vector>
#include <stdexcept>

#include "daemonize.hpp"
#include <csignal>

#define MAX_READ 50000

#define STDOUT 0
#define STDERR 1

#define READ_FD  0
#define WRITE_FD 1

#define PARENT_READ_STDOUT ( pipes[STDOUT][READ_FD] )
#define PARENT_READ_STDERR ( pipes[STDERR][READ_FD] )

#define CHILD_WRITE_STDOUT ( pipes[STDOUT][WRITE_FD]  )
#define CHILD_WRITE_STDERR ( pipes[STDERR][WRITE_FD]  )

class Command
{
	std::vector<std::string> arg;  //argumenty do exec
	std::string stdout;  //tekst z stdout komendy
	std::string stderr;  //tekst z stderr komendy
	int status;  //status wykonania komendy, mozna wydobyc kod exit
	int pipes[2][2];
	bool executed;
public:
	/*
	 * Treść komendy do wykonania
	 */
	Command(std::string command);

	/*
	 * Wykomuje komendę
	 */
	void execute();

	const std::string& getStderr() const
	{
		return stderr;
	}

	const std::string& getStdout() const
	{
		return stdout;
	}

	int getStatus() const
	{
		return status;
	}
};

#endif /* COMMAND_HPP_ */
