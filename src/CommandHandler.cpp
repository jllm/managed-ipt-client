#include "CommandHandler.hpp"

CommandHandler::CommandHandler(
	const std::string& clientPrivKeyFile,
	const std::string& adminPubKeyFile)
	: clientPrivateKey(KeyLoader::readRSAPrivateKey(clientPrivKeyFile))
	, adminPublicKey(KeyLoader::readRSAPublicKey(adminPubKeyFile))
	, isDryRun(false)
{
}

std::string CommandHandler::executeAndGetReply(const std::string& msg)
{
    BOOST_LOG_SEV(logger, info)<< "Received: " << msg;
	Response response;
    // Zamiana żadania na odpowiedni komunikat.
	try
	{
		Request request = Request::createRequest(msg);

		// Sprawdzenie podpisu
		if (!request.checkSign(adminPublicKey))
		{
			// Brzydkie.
			throw std::logic_error("Invalid signature");
		}

	    // Wykonanie.
		std::string commandString = std::string("iptables");

		if(request.getTable().compare(""))
		{
			commandString += " -t " + request.getTable();
		}

		commandString += " -" + request.getOperation();

		commandString += " " + request.getChain();

		commandString += " " + request.getOptionsRules();

		BOOST_LOG_SEV(logger, info)<< "Executing: " << commandString;

		if (!getDryRun())
		{
			Command command(commandString);

			command.execute();

			response.setCommandID(std::to_string(generateID()));

			const int status = command.getStatus();
			if(WIFEXITED(status) && WEXITSTATUS(status) == 0)  //czy zakonczyl sie poprzez exit(X);
			{
				response.setStatus("S");
				response.setOutput(command.getStdout());
			}
			else
			{
				response.setStatus("F");
				response.setOutput(command.getStderr());
			}
		}
		else
		{
			response.setStatus("S");
			response.setOutput("dry-run: " + commandString);
		}

		// Podpisanie odpowiedzi.
		response.sign(clientPrivateKey);

	}
	catch (const std::logic_error& e)
	{
		BOOST_LOG_SEV(logger, warning)<< "Invalid signature, ignoring...";

		response.setCommandID(std::to_string(generateID()));
		response.setStatus("F");
		response.setOutput("Invalid signature");
		response.sign(clientPrivateKey);
	}
	catch (const std::exception& e)
	{
		BOOST_LOG_SEV(logger, warning)<< "Unknown message received, ignoring...";

		response.setCommandID(std::to_string(generateID()));
		response.setStatus("F");
		response.setOutput("Unknown message");
		response.sign(clientPrivateKey);
	}

    return response.toString();
}

int CommandHandler::generateID()
{
	return rand() % 9000 + 1000;
}

void CommandHandler::setDryRun(bool isDryRun)
{
	this->isDryRun = isDryRun;
}

bool CommandHandler::getDryRun()
{
	return isDryRun;
}
