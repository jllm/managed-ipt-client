#pragma once

#include <string>

class InputHandler
{
public:
    virtual std::string executeAndGetReply(const std::string&) = 0;
    virtual ~InputHandler() {};
};
