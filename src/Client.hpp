#pragma once

#include "TCPServer.hpp"

#include <iostream>
#include <string>
#include <exception>

#include <cstring>
#include <cstdint>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

class Client : public TCPServer
{
public:
	Client(InputHandler&);
};
