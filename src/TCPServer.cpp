#include "TCPServer.hpp"

#include <iostream>
#include <string>
#include <cstring>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "../managed-ipt-common/src/exceptions/AcceptException.hpp"
#include "../managed-ipt-common/src/exceptions/BaseException.hpp"
#include "../managed-ipt-common/src/exceptions/BindException.hpp"
#include "../managed-ipt-common/src/exceptions/ConnectException.hpp"
#include "../managed-ipt-common/src/exceptions/ListenException.hpp"
#include "../managed-ipt-common/src/exceptions/NetException.hpp"
#include "../managed-ipt-common/src/exceptions/ReadException.hpp"
#include "../managed-ipt-common/src/exceptions/SocketException.hpp"
#include "../managed-ipt-common/src/exceptions/WriteException.hpp"

TCPServer::TCPServer(InputHandler& inputHandler)
    : inputHandler(inputHandler)
{
}

void TCPServer::start(int portNumber)
{
    int status;
    int sock = socket(AF_INET6, SOCK_STREAM, 0);
	if (sock == -1)
	{
		throw SocketException();
	}

	sockaddr_in6 agent_name;
	agent_name.sin6_family = AF_INET6;
	agent_name.sin6_addr = in6addr_any;
	agent_name.sin6_port = htons(portNumber);

	status = bind(sock, (struct sockaddr*) &agent_name, sizeof(agent_name));
	if (status == -1)
	{
		throw BindException();
	}

	status = listen(sock, 5);
	if (status == -1)
	{
		throw BindException();
	}

	while (true)
	{
		sockaddr_in6 agent_name;
		int addrlen = sizeof(agent_name);
		int msgsock = accept(sock,
							 (struct sockaddr*) &agent_name,
							 (socklen_t*) &addrlen);
		if (msgsock == -1)
		{
			throw AcceptException();
		}

		int rval;
		std::string packetBuffer;
		std::string partialPacketBuffer;
		size_t endOfPacketSignIndex;
		bool packetReadyToExecute = false;
		bool readFromSocket = true;
		do
		{
			if(readFromSocket)
				rval = read(msgsock, readBuffer, BUFFER_SIZE - 1);

			if (rval == -1)
			{
				throw ReadException();
			}
			// Nie chcę obsługiwać pustego napisu, który oznacza koniec
			// połączenia.
			else if (rval != 0)
			{
				if(readFromSocket)
					readBuffer[rval] = 0;
				else
					readBuffer[0] = 0;

				partialPacketBuffer += readBuffer;

				if(packetBuffer.length() > BUFFER_SIZE)  //zabezpieczenie przed sklejeniem zbyt duzego pakietu.
				{
					packetBuffer = "";
					packetReadyToExecute = false;
				}

				endOfPacketSignIndex = partialPacketBuffer.find_first_of("$");

				if(endOfPacketSignIndex == std::string::npos)
				{
					packetBuffer += partialPacketBuffer;
					partialPacketBuffer = "";
				}
				else
				{
					packetBuffer += partialPacketBuffer.substr(0, endOfPacketSignIndex + 1);
					partialPacketBuffer = partialPacketBuffer.substr(endOfPacketSignIndex + 1);

					packetReadyToExecute = true;
				}


				if(packetReadyToExecute)
				{
					std::string reply = inputHandler.executeAndGetReply(packetBuffer);
					write(msgsock, reply.c_str(), reply.size());

					packetBuffer = "";
					packetReadyToExecute = false;

					//jesli w partialPacketBuffer jest jeszcze jedna instrukcja to zamiast czytac cos nowego obslugujemy ja
					if(partialPacketBuffer.find_first_of("$") != std::string::npos)
						readFromSocket = false;
					else
						readFromSocket = true;
				}
            }
        } while (rval != 0);
        close(msgsock);
    }

    close(sock);
}
