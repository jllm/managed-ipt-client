/*
 * daemon.hpp
 *
 *  Created on: 16 maj 2014
 *      Author: jakub
 */

#pragma once

#include <csignal>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>

#include <stdexcept>
#include <fstream>

#include "../managed-ipt-common/src/defines.hpp"
#include "../managed-ipt-common/src/common.hpp"

namespace daemonize
{
	class daemon_already_running_exception: public std::exception
	{

	};

	class daemon_failed_to_stop_exception: public std::exception
	{

	};

	class daemon_not_running_exception: public std::exception
	{

	};

	/**
	 * Zapisuje pid procesu do pliku.
	 * @param fileName - nazwa pliku (ze ścieżką), do którego zostanie zapisane pid_t procesu.
	 */
	void save_pid(std::string fileName);

	void delete_pid_file();

	void check(std::string fileName);

	bool isDaemon();

	/**
	 * Zmienia proces w daemona, patrz: http://pl.wikipedia.org/wiki/Demon_(informatyka)
	 */
	void turn_into_daemon();

	/**
	 * Zatrzymuje daemona.
	 * @param fileName - plik zawieracjący pid daemona.
	 */
	void stop_daemon(std::string fileName);
}
