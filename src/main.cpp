#include <iostream>

#include <cstdlib>
#include <unistd.h>
#include <csignal>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <time.h>

#include "Client.hpp"
#include "CommandHandler.hpp"
#include "daemonize.hpp"

#include <boost/program_options.hpp>

#include "../managed-ipt-common/src/common.hpp"
#include "../managed-ipt-common/src/defines.hpp"
#include "../managed-ipt-common/src/KeyGen.hpp"
#include "../managed-ipt-common/src/exceptions/BaseException.hpp"

namespace
{
	Logger logger;
}

void kill_handler(int signum)
{
	BOOST_LOG_SEV(logger, info)<< "Caught signal: " << signum;
	BOOST_LOG_SEV(logger, info) << "Application will now exit.";
	exit(signum);
}

inline void install_signal_handlers()
{
	signal(SIGINT, kill_handler);
	signal(SIGSEGV, kill_handler);
	signal(SIGTERM, kill_handler);
	signal(SIGABRT, kill_handler);
}

int stop_daemon()
{
	try
	{
		daemonize::stop_daemon(CLIENT_PID_FILE);
		BOOST_LOG_SEV(logger, info)<< "Daemon stopped.";
		return EXIT_SUCCESS;
	}
	catch (const daemonize::daemon_failed_to_stop_exception& e)
	{
		BOOST_LOG_SEV(logger, warning)<< "Daemon not found or cannot be killed.";
		return EXIT_FAILURE;
	}
	catch (const daemonize::daemon_not_running_exception& e)
	{
		BOOST_LOG_SEV(logger, warning)<< "Daemon is not running.";
		return EXIT_FAILURE;
	}
	catch (const std::exception& e)
	{
		BOOST_LOG_SEV(logger, error)<< "Error occurred while stoping daemon.";
		common::print_exception(logger, e);
		return EXIT_FAILURE;
	}
}

void generateKeys(std::string privateKeyName)
{
	std::string publicKeyName = privateKeyName + ".pub";
	bool privKeyFileDoesntExist, pubKeyFileDoesntExist;
	if ((pubKeyFileDoesntExist = (access(publicKeyName.c_str(), F_OK) < 0)) &&
			(privKeyFileDoesntExist = (access(privateKeyName.c_str(), F_OK) < 0)))
	{
		KeyGen keygen;
		auto keys = keygen.generate();
		keygen.savePrivateKey(privateKeyName, keys->second);
		keygen.savePublicKey(publicKeyName, keys->first);
	}
	else
	{
		throw std::logic_error("RSA keys already exist. Delete them first");
	}
}

int main(int argc, char* argv[])
{
	namespace cm = common;
	namespace po = boost::program_options;
	srand(time(NULL));
	try
	{
		install_signal_handlers();

		cm::log::init();

		std::string programName("managed-ipt-client");
		std::string programNameVer(programName + " v0.90");
		std::string usage(
				"Usage:"
						"\t" + programName
						+ " -p port -c client-private-key -k admin-public-key [--daemon] [--silent] [--log-path path]\n"
								"\t" + programName + " --stop-daemon\n"
								"\t" + programName + " --key-gen private-key-name\n");

		std::string filename("file");
		po::options_description general("General options");
		general.add_options()
		("help,h", "Produce help message.")
		("port,p", po::value<int>()->required()->value_name("port"), "Listening port.")
		("client-private-key,c", po::value<std::string>()->value_name(filename)->required(),
				"A file with client's private key")
		("admin-public-key,k", po::value<std::string>()->value_name(filename)->required(),
				"A file with admin's public key")
		("key-gen,g", po::value<std::string>()->value_name("private-key-name"),
				"Generate new client RSA keys. Does not overwrite existing keys.")
		("daemon,d", po::bool_switch()->default_value(false), "Start process as daemon.")
		("silent,s", po::bool_switch()->default_value(false), "Silent mode.")
		("dry-run", po::bool_switch()->default_value(false), "Dry run command")
		("log-path,l", po::value<std::string>()->value_name("path"), "Logs directory path.")
				;

		po::options_description special("Special options");
		special.add_options()
		("full-help,H", "Print full help message.")
		("debug", po::bool_switch()->default_value(false), "Print debug logs.")
				;

		po::options_description hidden("Hidden options");
		hidden.add_options()
		("stop-daemon,E", po::bool_switch()->default_value(false), "Stop daemon.")
				;

		po::options_description allowed("Allowed options");
		allowed.add(general).add(special);

		po::options_description all("All options");
		all.add(allowed).add(hidden);

		po::positional_options_description p;
		p.add("port", -1);

		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).
				options(all).positional(p).run(), vm);

		if (vm.count("full-help"))
		{
			std::cout << programNameVer << "\n\n";
			std::cout << usage << "\n";
			std::cout << general << "\n";
			std::cout << special;
			return EXIT_SUCCESS;
		}
		if (vm.count("help") || argc == 1)
		{
			std::cout << programNameVer << "\n\n";
			std::cout << usage << "\n";
			std::cout << general;
			return EXIT_SUCCESS;
		}

		if (vm.count("key-gen"))
		{
			try
			{
				generateKeys(vm["key-gen"].as<std::string>());
				return EXIT_SUCCESS;
			}
			catch (std::logic_error& e)
			{
				BOOST_LOG_SEV(logger, error)<< "Error:" << e.what() << ".";
				return EXIT_FAILURE;
			}
		}

		if (vm["stop-daemon"].as<bool>())
		{
			return stop_daemon();
		}

		// NOTIFY - dopiero tutaj sprawdzana program rzuca wyjątki
		// jeżeli coś jest nie tak z opcjami uruchomienia.
		po::notify(vm);

		if (vm["debug"].as<bool>())
		{
			cm::log::set_severity_level(debug);
		}

		if (vm["silent"].as<bool>())
		{
			// SILENT LOGS
			cm::log::silent();
		}

		if (vm.count("port"))
		{
			bool dryRun = vm["dry-run"].as<bool>();
			if (!dryRun && !common::permissions::isRoot())
			{
				BOOST_LOG_SEV(logger, warning)<< "You are not root.";
				BOOST_LOG_SEV(logger, warning)<< "You may not be able to use iptables correctly.";
			}

			int portNumber = vm["port"].as<int>();
			std::string clientPrivKeyFile = vm["client-private-key"].as<std::string>();
			std::string adminPubKeyFile = vm["admin-public-key"].as<std::string>();

			CommandHandler commandHandler(clientPrivKeyFile, adminPubKeyFile);
			commandHandler.setDryRun(dryRun);

			Client client(commandHandler);

			// Przejście do trybu daemon przed otwarciem plików.
			if (vm["daemon"].as<bool>())
			{
				try
				{
					if(!common::permissions::check<2>(
							{CLIENT_PID_FILE_DIR, CLIENT_LOG_FILE_DIR},
							{W_OK | R_OK, W_OK | R_OK}))
					{
						BOOST_LOG_SEV(logger, error)<< "Permission denied error.";
						BOOST_LOG_SEV(logger, error)<< "You must have rights to write/read to:";
						BOOST_LOG_SEV(logger, error)<< "\t" << CLIENT_PID_FILE_DIR;
						BOOST_LOG_SEV(logger, error)<< "\t" << CLIENT_LOG_FILE_DIR;
						return EXIT_FAILURE;
					}

					daemonize::check(CLIENT_PID_FILE);
					daemonize::turn_into_daemon();
					daemonize::save_pid(CLIENT_PID_FILE);
					cm::log::to_file(CLIENT_LOG_FILE);
					BOOST_LOG_SEV(logger, info)<< "Client started in daemon mode.";
				}
				catch (const daemonize::daemon_already_running_exception& e)
				{
					BOOST_LOG_SEV(logger, error)<< "Daemon is already running.";
					return EXIT_FAILURE;
				}
				catch (const std::exception& e)
				{
					BOOST_LOG_SEV(logger, error)<< "Critical error occurred while turning into daemon.";
					cm::print_exception(logger, e);
					return EXIT_FAILURE;
				}
			}
			else
			{
				BOOST_LOG_SEV(logger, info) << "Client started.";

			}
			BOOST_LOG_SEV(logger, info)<< "Port: " << portNumber;
			try
			{
				client.start(portNumber);
				BOOST_LOG_SEV(logger, info)<< "Program ended.";
				return EXIT_SUCCESS;
			}
			catch (const BaseException& e)
			{
				BOOST_LOG_SEV(logger, error)<< "Connection error occurred.";
				BOOST_LOG_SEV(logger, error)<< "Application will now exit.";
				return EXIT_FAILURE;
			}
		}
	}
	catch (const po::error& e)
	{
		// Jeżeli to nastąpi to na pewno przed wejściem w stan daemon.
		std::cerr << "Program options error occurred: " << e.what() << ".\n";
		std::cerr << "Use --help option to see help.\n";
		return EXIT_FAILURE;
	}
	catch (const std::exception& e)
	{
		BOOST_LOG_SEV(logger, fatal)<< "Unhandled exception reached the top of main.";
		cm::print_exception(logger, e);
		BOOST_LOG_SEV(logger, fatal)<< "Application will now exit.";
		return EXIT_FAILURE;
	}
	catch (...)
	{
		BOOST_LOG_SEV(logger, fatal)<< "Unhandled exception of unknown type reached the top of main.";
		BOOST_LOG_SEV(logger, fatal)<< "Application will now exit.";
		return EXIT_FAILURE;
	}
}
