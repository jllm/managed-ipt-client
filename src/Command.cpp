#include "Command.hpp"

Command::Command(std::string text) :
		executed(false)
{
	boost::char_separator<char> sep(" ", "", boost::drop_empty_tokens);
	boost::tokenizer<boost::char_separator<char> > tok(text, sep);

	for (std::string atom : tok)
		arg.push_back(atom);

	status = 0;
}

void Command::execute()
{
	if (executed)
		return;

	pipe(pipes[STDOUT]);
	pipe(pipes[STDERR]);

	// Jeżeli program jest uruchomiony w trybie
	// daemona to trzeba przywrócić obsługę
	// sygnału SGICHLD na czas wykonania
	// procesu potomnego.
	if (daemonize::isDaemon())
	{
		signal(SIGCHLD, SIG_DFL);
	}

	if (fork())  //parent
	{
		close(CHILD_WRITE_STDOUT);
		close(CHILD_WRITE_STDERR);

		char buffer[MAX_READ];
		int count;

		//wczytywanie stdout
		count = read(PARENT_READ_STDOUT, buffer, sizeof(buffer) - 1);
		if (count >= 0)
		{
			buffer[count] = 0;
			stdout = buffer;
		}
		else
		{
			throw std::runtime_error("IO Error");
		}

		//wczytywanie stderr
		count = read(PARENT_READ_STDERR, buffer, sizeof(buffer) - 1);
		if (count >= 0)
		{
			buffer[count] = 0;
			stderr = buffer;
		}
		else
		{
			throw std::runtime_error("IO Error");
		}

		close(PARENT_READ_STDOUT);
		close(PARENT_READ_STDERR);
	}
	else  //child
	{
		dup2(CHILD_WRITE_STDOUT, STDOUT_FILENO);
		dup2(CHILD_WRITE_STDERR, STDERR_FILENO);

		close(PARENT_READ_STDOUT);
		close(PARENT_READ_STDERR);
		close(CHILD_WRITE_STDOUT);
		close(CHILD_WRITE_STDERR);

		char **argTab = new char*[arg.size()];

		int i = 0;
		for (std::string str : arg)
		{
			char* strBuf = new char[50];  //arbitralnie 50 znaków na 1 człon polecenia
			if (str.length() > 49)
				exit(31);   //za dlugi człon

			strcpy(strBuf, str.c_str());
			argTab[i] = strBuf;
			++i;
		}
		argTab[i] = 0;

		execvp(argTab[0], argTab);

		exit(15);   //polecenie exec nie wykonało się poprawnie
	}

	wait(&status);
	executed = true;

	if (daemonize::isDaemon())
	{
		signal(SIGCHLD, SIG_IGN);
	}

	close(PARENT_READ_STDOUT);
	close(PARENT_READ_STDERR);
}
