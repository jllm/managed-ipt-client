#pragma once

#include "CommandHandler.hpp"
#include <algorithm>

class TCPServer
{
	static const int BUFFER_SIZE = 10000;
	char readBuffer[BUFFER_SIZE];
	InputHandler& inputHandler;
public:
    TCPServer(InputHandler&);
    virtual void start(int);
};
