#pragma once

#include <stdlib.h>
#include <sys/wait.h>

#include "InputHandler.hpp"
#include "../managed-ipt-common/src/Request.hpp"
#include "../managed-ipt-common/src/Response.hpp"
#include "../managed-ipt-common/src/common.hpp"
#include "../managed-ipt-common/src/KeyLoader.hpp"
#include "Command.hpp"

class CommandHandler : public InputHandler
{
private:
	/** Klucz prywatny klienta do podpisania. */
	const CryptoPP::RSA::PrivateKey clientPrivateKey;
	/** Klucz publiczny admina do weryfikacji podpisu. */
	const CryptoPP::RSA::PublicKey adminPublicKey;
	/** Czy wykonywać komendę */
	bool isDryRun;
	Logger logger;

	/**	Generuje Id z zakresu 1000 9999 */
	int generateID();

public:
	CommandHandler(const std::string&, const std::string&);
    ~CommandHandler() {}
    std::string executeAndGetReply(const std::string& request);
	void setDryRun(bool);
	bool getDryRun();
};
